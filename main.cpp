#include <stdio.h>
#include <string>
#include <pthread.h>
#include <algorithm>
#include <mutex>

// Boost
#include <boost/thread.hpp>

// PCL point cloud manipulationg
//#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree_flann.h>

// openCV library for multi-threading, hough transformation
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/core/operations.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// CGAL, library for 2-D, 3-D set boolean operation on polygons
#include <CGAL/Exact_predicates_exact_constructions_Kernel.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Polygon_with_holes_2.h>
#include <CGAL/Polygon_set_2.h>
#include <CGAL/IO/Gps_iostream.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Nef_polyhedron_3.h>
#include <CGAL/IO/Nef_polyhedron_iostream_3.h>

// utility for reading laser inputs
#include <io/data/fss/fss_io.h>
#include <geometry/system_path.h>
#include <util/error_codes.h>

#include <fsc/fsc.h>
#include <pch/pch.h>
#include <constants/constants.h>

// all parameters are measured in meters
std::string DS_FILE_PATH;
// represents upper (positive) bound and lower (negative) bound for histogram 
// formation
double half_range; 
double min_x;
double min_y;

boost::mutex mutex;

std::string backpack_config_file_path;
std::string mad_file_path;
std::vector<std::string> fss_file_paths;

fss::reader_t infile;
fss::frame_t frame; // cloud point
transform_t fss_pose; // scanner position
system_path_t scanner_path;

// PCL cloud
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);

// gadgets
template<typename T>
int write_vector_to_file(std::vector<T> v, std::string file_name);
int write_eigen_VectorXd_to_file(Eigen::VectorXd v, std::string file_name);
void wait();

/* data loading */
int load_data_set_paths();
int load_backpack_config();
int load_scanner_path();
int load_data_into_point_cloud();

// operation functions
// these functions might cause memory leaks
// half_range is dynamically defined here
std::vector<double> construct_height_histogram(); 
void construct_gaussian_smoothing_operator(std::vector<double> &v);
void find_peaks(std::vector<double> v, std::vector<int> &peaks);
void filter_histogram(std::vector<double> &filtered_histogram, std::vector<double> 
  histogram, std::vector<double> gaussian_smoothing_filter);

// helper functions
int height_to_interval(double height);
int interval_to_lower_bound(int interval);
int interval_to_upper_bound(int interval);
int height_to_free_space_interval(double height);
template<typename T>
void free_vector(std::vector<T> &v);
// return min x, min y, max x, max y
std::vector<double> get_extremum_in_point_vector(
  std::vector< std::vector<pcl::PointXYZ> > layers, 
  std::vector<int> peaks);

void compute_best_polygon_parallel(
  double interval_idx,
  double &curr_value, 
  double &curr_increase,
  bool &isAdditive, 
  CGAL::Polygon_2<Kernel_2> &p, 
  CGAL::Polygon_2<Kernel_2> &best, 
  fsc::matrix3d &free_space_scores, 
  CGAL::Polygon_set_2<Kernel_2> &curr_polygon_set,
  std::vector< std::vector<pcl::PointXYZ> > &layers_points);

void compute_score_for_nef_polyhedron_parallel(
  CGAL::Nef_polyhedron_3<Kernel_3> &curr_nef_polyhedron,
  CGAL::Nef_polyhedron_3<Kernel_3> &p,
  CGAL::Nef_polyhedron_3<Kernel_3> &best,
  double &value,
  double &curr_value,
  double &curr_increase,
  bool &isAdditive,
  fsc::matrix3d &free_space_scores);

int main( int argc, const char* argv[] )
{
  CGAL::set_pretty_mode(std::cout);
  /* load file containing data paths from command line */
  if (argc != 2)
  {
    std::cerr << "ERROR! wrong number of arguments, expected 1 argument" << std::endl;
    return -1;
  }
  else
    DS_FILE_PATH = argv[1];

  /* load environment data */
  if (load_data_set_paths()) return -1;
  if (load_scanner_path()) return -1;
  if (load_backpack_config()) return -1;
  if (load_data_into_point_cloud()) return -1;

  /* 
  construct the histogram of laser point distribution on different height 
  intervals 
  */
  std::vector<double> histogram = construct_height_histogram();
  write_vector_to_file(histogram, "histogram");

  /* smoothen histogram using gaussian smoothing */
  std::vector<double> gaussian_smoothing_filter(2*height_to_interval(half_range)-1, 0);
  construct_gaussian_smoothing_operator(gaussian_smoothing_filter);
  write_vector_to_file(gaussian_smoothing_filter, "gaussian_smoothing_operator");
  std::vector<double> filtered_histogram(histogram.size(), 0.0);
  filter_histogram(filtered_histogram, histogram, gaussian_smoothing_filter);
  write_vector_to_file(filtered_histogram, "filtered_histogram");

  std::vector<int> peaks;
  find_peaks(filtered_histogram, peaks);

  free_vector(histogram);
  free_vector(filtered_histogram);
  free_vector(gaussian_smoothing_filter);

  write_vector_to_file(peaks, "peaks_index_in_histogram");

  std::vector< std::vector<pcl::PointXYZ> > layers_points(height_to_interval(half_range), std::vector<pcl::PointXYZ>());
  std::cout << "number of layer: " << layers_points.size() << std::endl;

  /* distribute laser points into layers */
  for (pcl::PointXYZ point : *cloud_ptr)
  {
    if (height_to_interval(point.z) >= height_to_interval(half_range) or height_to_interval(point.z) < 0) 
    {
      std::cerr << "Error! cannot map point: z->" << point.z 
        << " into interval->" << height_to_interval(point.z) << std::endl;
    }
    else
    {
      layers_points[height_to_interval(point.z)].push_back(point);
    }
  }

  std::vector<double> extremum = 
    get_extremum_in_point_vector(layers_points, peaks);
  min_x = extremum[0];
  min_y = extremum[1];
  double max_x = extremum[2];
  double max_y = extremum[3];
  /* all points will need to map through this operation */
  int rows = ceil( (max_x - min_x) / constants::RESOLUTION );
  int cols = ceil( (max_y - min_y) / constants::RESOLUTION );

  fsc::matrix3d free_space_scores(rows, cols, 
    height_to_free_space_interval(half_range));
  std::vector<fsc::matrix3d> thread_matrices;
  /* calculate free space score */
  int count_line_incremented = 0;
  for (std::string file_path : fss_file_paths)
  {
    int num_frames;
    fss::reader_t infile;
    fss::frame_t frame; // cloud point
    transform_t fss_pose; // scanner position

    /* retrieve scanner data */
    infile.set_correct_for_bias(true);
    if(infile.open(file_path))
    {
      /* report error */
      std::cerr << "Error!  Unable to open fss file: "
         << file_path << std::endl;
      return -1;
    }

    /* iterate over frames */
    num_frames = infile.num_frames();
    for(int i = 0; i < num_frames; i++)
    {
      /* get frame */
      if (infile.get(frame, i))
      {
        /* report error */
        std::cerr << "Error! Can't retrieve frame at time " << i 
          << " for " << infile.scanner_name() << std::endl;
        return -1;
      }

      if(scanner_path.compute_transform_for(fss_pose, frame.timestamp, 
        infile.scanner_name()))
      {
        /* report error */
        std::cerr << "Error! Can't compute fss pose in frame " << i << " at time "
           << frame.timestamp << " for "
           << infile.scanner_name() << std::endl;
        return -1;
      }

      for (fss::point_t pt : frame.points)
      {
        // construct the line
        Eigen::Vector3d point;
        point[0] = (fss_pose.T[0]-min_x) / constants::RESOLUTION;
        point[1] = (fss_pose.T[1]-min_y) / constants::RESOLUTION;
        point[2] = (fss_pose.T[2]+half_range) / constants::RESOLUTION;
        Eigen::Vector3d displacement;
        displacement[0] = pt.x / constants::RESOLUTION;
        displacement[1] = pt.y / constants::RESOLUTION;
        displacement[2] = pt.z / constants::RESOLUTION;
        fsc::line_3d line(point, displacement);
        free_space_scores.increment_for_line(line);
        count_line_incremented++;
      }
      /* for each frame, record height for each point */
    }
    infile.close();
  }
  std::cout << "increment voxel count for " << count_line_incremented 
    << " laser points" << std::endl;
  free_space_scores.enforce_max_value(constants::VOXEL_MAX_VALUE);
  free_space_scores.compute_negative_free_space_score(constants::RESOLUTION, constants::NEGATIVE_VOXEL_MULTIPLIER);

  // convert points into voxel coordination
  // this will mutate points in the layerwise <pointXYZ> vector as well
  cloud_ptr->clear();
  for (std::vector<pcl::PointXYZ> &layer_point : layers_points)
  {
    for (pcl::PointXYZ &point : layer_point)
    {
      point.x = floor((point.x - min_x) / constants::RESOLUTION);
      point.y = floor((point.y - min_y) / constants::RESOLUTION);
      point.z = floor((point.z + half_range) / constants::RESOLUTION);
      cloud_ptr->push_back(point);
    } 
  }

  std::vector<Traits_2::General_polygon_2> polygons_for_all_layers;
  /* hough transformation, 2d recostruction */
  for (double interval_idx : peaks)
  {
    std::cout << "interval " << interval_idx << ", from: " << interval_idx * 
      constants::HISTOGRAM_INTERVAL_SIZE - half_range << " to: " << (interval_idx+1) * 
      constants::HISTOGRAM_INTERVAL_SIZE - half_range <<  ", number of points: " << 
      layers_points[interval_idx].size() << std::endl;
    if (layers_points[interval_idx].size() == 0) 
    {
      std::cerr << "ERROR! hough transform: interval layer has no points" << 
        std::endl;
      return -1;
    }
       
    cv::Mat image = cv::Mat::zeros(rows, cols, CV_8UC1);
    for (pcl::PointXYZ point: layers_points[interval_idx])
    {
      int row = point.y;
      int col = point.x;
      // printf ("col: %d/%d, row: %d/%d\n", col,cols-1,row,rows-1);
      if (fsc::in_range(row, 0, rows-1) and fsc::in_range(col, 0, cols-1) and (int)(image.at<uchar>(row,col)) < 255) 
        image.at<uchar>(row,col) = (int)(image.at<uchar>(row,col)) + 1;
    }

    std::vector<cv::Vec4i> houghLines;
    // houghLines is a vector of lines
    // each line is a vector of 4 elements, x1,y1,x2,y2
    cv::HoughLinesP(image, houghLines, 1, CV_PI/180, constants::HOUGH_TRANSFORM_THRESHOLD, 30, 10);
    std::cout << "interval " << interval_idx << ": " << houghLines.size() << 
      " lines" << std::endl;

    std::vector<fsc::line_2d> lines;
    for (cv::Vec4i l : houghLines)
    {
      fsc::line_2d new_line(l[0], l[1], l[2], l[3]);
      lines.push_back(new_line);
    }

    // generate 2-D layerwise primitives
    std::vector<Traits_2::General_polygon_2> polygons;
    for (fsc::line_2d l1 : lines)
    {
      for (fsc::line_2d l2 : lines)
      {
        if (l1 == l2) break;
        // permutations of any 2 lines
        if (l1.is_perpendicular_to(l2))
        {
          // 2 lines permutations, l1 perpendicular l2
          fsc::line_2d line1l(l1.point, l1.displacement);
          fsc::line_2d line1r(l2.point, l1.displacement);
          fsc::line_2d line2l(l2.point, l2.displacement);
          fsc::line_2d line2r(l1.point, l2.displacement);
          polygons.push_back(pch::polygonWithLines(line1l,line1r,line2l,line2r));
          line1l.point = l1.point;
          line1r.point = l2.point;
          line2l.point = l1.point;
          line2r.point = l1.end_point();
          polygons.push_back(pch::polygonWithLines(line1l,line1r,line2l,line2r));
          line1l.point = l2.point;
          line1r.point = l2.end_point();
          line2l.point = l1.point;
          line2r.point = l2.point;
          polygons.push_back(pch::polygonWithLines(line1l,line1r,line2l,line2r));
          line1l.point = l2.point;
          line1r.point = l2.end_point();
          line2l.point = l1.point;
          line2r.point = l2.end_point();
          polygons.push_back(pch::polygonWithLines(line1l,line1r,line2l,line2r));
          line1l.point = l1.point;
          line1r.point = l2.end_point();
          line2l.point = l2.point;
          line2r.point = l1.point;
          polygons.push_back(pch::polygonWithLines(line1l,line1r,line2l,line2r));
          line1l.point = l1.point;
          line1r.point = l2.end_point();
          line2l.point = l1.point;
          line2r.point = l1.end_point();
          polygons.push_back(pch::polygonWithLines(line1l,line1r,line2l,line2r));
          line1l.point = l1.point;
          line1r.point = l2.point;
          line2l.point = l2.point;
          line2r.point = l1.end_point();
          polygons.push_back(pch::polygonWithLines(line1l,line1r,line2l,line2r));
          line1l.point = l2.point;
          line1r.point = l2.end_point();
          line2l.point = l2.point;
          line2r.point = l1.end_point();
          polygons.push_back(pch::polygonWithLines(line1l,line1r,line2l,line2r));
          line1l.point = l1.point;
          line1r.point = l2.end_point();
          line2l.point = l2.point;
          line2r.point = l1.end_point();
          polygons.push_back(pch::polygonWithLines(line1l,line1r,line2l,line2r));
        }
        else if (l1.is_parallel_to(l2))
        {
          // 2 lines permutations, l1 parallel l2
          fsc::line_2d line3l = l1.perpendicular_line();
          fsc::line_2d line3r = l2.perpendicular_line();
          polygons.push_back(pch::polygonWithLines(l1,l2,line3l,line3r));
          line3r.point = l1.end_point();
          polygons.push_back(pch::polygonWithLines(l1,l2,line3l,line3r));
          line3r.point = l2.end_point();
          polygons.push_back(pch::polygonWithLines(l1,l2,line3l,line3r));
          line3l.point = l2.point;
          line3r.point = l1.end_point();
          polygons.push_back(pch::polygonWithLines(l1,l2,line3l,line3r));
          line3r.point = l2.end_point();
          polygons.push_back(pch::polygonWithLines(l1,l2,line3l,line3r));
          line3l.point = l1.end_point();
          line3r.point = l2.end_point();
          polygons.push_back(pch::polygonWithLines(l1,l2,line3l,line3r));
          for (fsc::line_2d l3 : lines)
          {
            // 3 lines formation
            if (l3 == l1 or l3 == l2) 
              break;
            if (l3.is_perpendicular_to(l1))
            {
              // valid 3 lines permutations, l1,l2 perpendicular l3
              fsc::line_2d line3p(l3.point, l3.displacement);
              line3p.point = l1.point;
              polygons.push_back(pch::polygonWithLines(l1,l2,l3,line3p));
              line3p.point = l1.end_point();
              polygons.push_back(pch::polygonWithLines(l1,l2,l3,line3p));
              line3p.point = l2.point;
              polygons.push_back(pch::polygonWithLines(l1,l2,l3,line3p));
              line3p.point = l2.end_point();
              polygons.push_back(pch::polygonWithLines(l1,l2,l3,line3p));
              for (fsc::line_2d l4 : lines)
              {
                // 4 lines formation  
                if (l4 == l1 or l4 == l2 or l4 == l3) break;
                if (l4.is_parallel_to(l3))
                {
                  // 4 lines permutations, l1,l2 perpendicular l3,4
                  polygons.push_back(pch::polygonWithLines(l1,l2,l3,l4));
                }
              }
            }
          }
        }   
      }   
    }
    std::cout << "interval " << interval_idx << ": " << polygons.size() << 
      " planar primitives before pruning" << std::endl;
    pch::prune(polygons);
    std::cout << "interval " << interval_idx << ": " << polygons.size() << 
      " planar primitives after pruning" << std::endl;

    // Display the lines in a window
    if ( constants::DEBUGGING_DISPLAY_HOUGH_TRANSFORM_LINES )
    {
      cv::Mat line_mat = cv::Mat::zeros(rows, cols, CV_8UC1);
      for( size_t i = 0; i < houghLines.size(); i++ )
      {
          line( line_mat, cv::Point(houghLines[i][0], houghLines[i][1]),
            cv::Point(houghLines[i][2], houghLines[i][3]), cvScalar(255,255,255));
      } 
      std::string name = "Detected Lines in interval " + 
        std::to_string(interval_idx);
      cv::namedWindow( name, cv::WINDOW_AUTOSIZE );
      cv::imshow( name, line_mat );
      wait();
      cv::destroyWindow( name ); 
    }

    CGAL::General_polygon_set_2<Traits_2> curr_polygon_set;
    double curr_value = 0.0;
    double value = 0.0;
    double curr_increase = 0.0;
    if (polygons.size() > 0)
    {
      
      do {
        CGAL::General_polygon_set_2<Traits_2> addition_polygon_set;
        CGAL::General_polygon_set_2<Traits_2> subtraction_polygon_set;
        Traits_2::General_polygon_2 best;
        bool isAdditive;
        curr_value = value;
        curr_increase = 0.0;
        std::cout << "curr_value: " << curr_value << std::endl;
        std::cout << "value: " << value << std::endl;
        std::cout << "curr_increase: " << curr_increase << std::endl;
        for (Traits_2::General_polygon_2 p : polygons)
        {
          addition_polygon_set = CGAL::General_polygon_set_2<Traits_2> (curr_polygon_set);
          addition_polygon_set.join(p);
          double f1value = constants::estimation_2d_1_weight * 
            pch::estimation_function_2d_1(addition_polygon_set, 
            free_space_scores, interval_idx);
          double f2value = constants::estimation_2d_2_weight * 
            pch::estimation_function_2d_2(addition_polygon_set,
            layers_points[interval_idx], interval_idx);
          double f3value = constants::estimation_2d_3_weight * 
            pch::estimation_function_2d_3(addition_polygon_set,
            layers_points[interval_idx], interval_idx);
          double value = f1value + f2value + f3value;
          if (value - curr_value > curr_increase) 
          {
            best = p;
            isAdditive = true;
            curr_increase = value - curr_value;
            if (constants::estimation_2d_print_increase)
              std::cout << "current increase: " << curr_increase << std::endl;
          }

          subtraction_polygon_set = CGAL::General_polygon_set_2<Traits_2> (curr_polygon_set);
          subtraction_polygon_set.difference(p);
          f1value = constants::estimation_2d_1_weight * 
            pch::estimation_function_2d_1(subtraction_polygon_set, 
            free_space_scores, interval_idx);
          f2value = constants::estimation_2d_2_weight * 
            pch::estimation_function_2d_2(subtraction_polygon_set,
            layers_points[interval_idx], interval_idx);
          f3value = constants::estimation_2d_3_weight * 
            pch::estimation_function_2d_3(subtraction_polygon_set,
            layers_points[interval_idx], interval_idx);
          value = f1value + f2value + f3value;
          if (value - curr_value > curr_increase) 
          {
            best = p;
            isAdditive = false;
            curr_increase = value - curr_value;
            if (constants::estimation_2d_print_increase)
              std::cout << "current increase: " << curr_increase << std::endl;
          }
        }


        if (curr_increase > 0)
        {
          polygons_for_all_layers.push_back(Traits_2::General_polygon_2(best));

          if (isAdditive)
          {
            curr_polygon_set.join(best);
          }
          else
          {
            curr_polygon_set.difference(best);
          }
        }

        // curr_increase = 0.0;
        // curr_value = value;
        // bool isAdditive;
        // CGAL::Polygon_2<Kernel_2> best;
        // std::vector<boost::thread> threads;
        // for(int thread_id=0; thread_id < polygons.size(); thread_id++)
        // {
        //   std::vector<double> v;
        //   v.push_back((double) thread_id);
        //   v.push_back(interval_idx);
        //   threads.push_back(
        //     boost::thread(compute_best_polygon_parallel,
        //       interval_idx,
        //       boost::ref(curr_value),
        //       boost::ref(curr_increase),
        //       boost::ref(isAdditive),
        //       std::ref(polygons[thread_id]),
        //       boost::ref(best),
        //       boost::ref(free_space_scores),
        //       boost::ref(curr_polygon_set),
        //       boost::ref(layers_points))
        //     );
        // }

        // for(int thread_id=0; thread_id < polygons.size(); thread_id++)
        // {
        //   threads[thread_id].join();
        // }

        // if (curr_increase > 0)
        // {
        //   if (isAdditive)
        //   {
        //     polygons_for_all_layers.push_back(CGAL::Polygon_2<Kernel_2>(best));
        //     curr_polygon_set.join(best);
        //   }
        //   else
        //   {
        //     curr_polygon_set.difference(best);
        //   }
        // }
        printf("polygon set at layer %d contains %lu polygons now\n", (int) interval_idx, curr_polygon_set.number_of_polygons_with_holes());
        std::cout << curr_polygon_set << std::endl;
        value = curr_value + curr_increase;
      } while (curr_increase >= constants::estimation_2d_pursue_threshold_value);
    }
    printf("polygon set at layer %d contains %lu polygons in total\n", (int) interval_idx, curr_polygon_set.number_of_polygons_with_holes());
    std::cout << curr_polygon_set << std::endl;
  }

  // std::vector<CGAL::Polygon_2<Kernel_2>> polygons_for_all_layers;
  // double interval_idx : peaks
  std::vector<CGAL::Nef_polyhedron_3<Kernel_3>> nef_polyhedrons;
  for (Traits_2::General_polygon_2 p : polygons_for_all_layers)
  {
    for (int h1: peaks)
    {
      for (int h2: peaks)
      {
        if (h1 >= h2) continue;
        CGAL::Polyhedron_3<Kernel_3> out;
        if (pch::make_cube_3(out, p, h1, h2))
        {
          nef_polyhedrons.push_back(CGAL::Nef_polyhedron_3<Kernel_3>(out));
        }
      }
    }
  }

  if (nef_polyhedrons.size() == 0)
  {
    std::cout << "no candidate primitive for 3d calculation" << std::endl;
    return 0;
  }
  else
  {
    std::cout << "number of 3d primitives: " << nef_polyhedrons.size() << std::endl;
  }

  CGAL::Nef_polyhedron_3<Kernel_3> curr_nef_polyhedron;
  double curr_value = 0.0;
  double value = 0.0;
  double curr_increase = 0.0;
  do
  {
    curr_value = value;
    curr_increase = 0.0;
    std::cout << "curr_value: " << curr_value << std::endl;
    std::cout << "value: " << value << std::endl;
    std::cout << "curr_increase: " << curr_increase << std::endl;
    bool isAdditive = false;
    CGAL::Nef_polyhedron_3<Kernel_3> best;

    // std::vector<boost::thread> threads;
    // for(int thread_id=0; thread_id < nef_polyhedrons.size(); thread_id++)
    // {
    //   threads.push_back(
    //     boost::thread(compute_score_for_nef_polyhedron_parallel,
    //       boost::ref(curr_nef_polyhedron),
    //       boost::ref(nef_polyhedrons[thread_id]),
    //       boost::ref(best),
    //       boost::ref(value),
    //       boost::ref(curr_value),
    //       boost::ref(curr_increase),
    //       boost::ref(isAdditive),
    //       boost::ref(free_space_scores)
    //       )
    //     );
    // }

    // for(int thread_id=0; thread_id < nef_polyhedrons.size(); thread_id++)
    // {
    //   threads[thread_id].join();
    // }

    for (CGAL::Nef_polyhedron_3<Kernel_3> p: nef_polyhedrons)
    {
      CGAL::Nef_polyhedron_3<Kernel_3> res = curr_nef_polyhedron + p;
      if (curr_nef_polyhedron == res) continue;
      double f1value = constants::estimation_3d_1_weight * 
        pch::estimation_function_3d_1(res, free_space_scores);
      double f2value = constants::estimation_3d_2_weight * 
        pch::estimation_function_3d_2(res, cloud_ptr);
      double f3value = constants::estimation_3d_3_weight * 
        pch::estimation_function_3d_3(res);
      double new_value = f1value + f2value + f3value;
      if (constants::estimation_3d_print_final_value)
      {
        std::cout << "value: " << new_value << std::endl;
      }
      if (new_value > value)
      {
        curr_increase = new_value - curr_value;
        value = new_value;
        isAdditive = true;
        best = p;
        std::cout << "curr_increase: " << curr_increase << std::endl;
      }
    }

    for (CGAL::Nef_polyhedron_3<Kernel_3> p: nef_polyhedrons)
    {
      CGAL::Nef_polyhedron_3<Kernel_3> res = curr_nef_polyhedron - p;
      if (curr_nef_polyhedron == res) continue;
      double f1value = constants::estimation_3d_1_weight * 
        pch::estimation_function_3d_1(res, free_space_scores);
      double f2value = constants::estimation_3d_2_weight * 
        pch::estimation_function_3d_2(res, cloud_ptr);
      double f3value = constants::estimation_3d_3_weight * 
        pch::estimation_function_3d_3(res);
      double new_value = f1value + f2value + f3value;
      if (constants::estimation_3d_print_final_value)
      {
        std::cout << "value: " << new_value << std::endl;
      }
      if (new_value > value)
      {
        curr_increase = new_value - curr_value;
        value = new_value;
        isAdditive = false;
        best = p;
        std::cout << "curr_increase: " << curr_increase << std::endl;
      }
    }

    if (curr_increase > 0)
    {
      if (isAdditive)
      {
        curr_nef_polyhedron = curr_nef_polyhedron + best;
      }
      else
      {
        curr_nef_polyhedron = curr_nef_polyhedron - best;
      }
    }
    curr_value = value;
    std::cout << "current: " << std::endl;
    std::cout << curr_nef_polyhedron << std::endl;
    std::cout << "points: " << std::endl;
    for (auto vi = curr_nef_polyhedron.vertices_begin();vi != curr_nef_polyhedron.vertices_end(); vi++) 
    {
      std::cout << vi->point() << std::endl; 
    } 
    
  } while (curr_increase >= constants::estimation_3d_pursue_threshold_value);

  std::cout << "=================================" << std::endl;
  std::cout << "output: " << std::endl;
  std::cout << curr_nef_polyhedron << std::endl;
  std::cout << "points: " << std::endl;
  for (auto vi = curr_nef_polyhedron.vertices_begin();vi != curr_nef_polyhedron.vertices_end(); vi++) 
  {
    std::cout << vi->point() << std::endl; 
  } 
  return 0;
}

void compute_score_for_nef_polyhedron_parallel(
  CGAL::Nef_polyhedron_3<Kernel_3> &curr_nef_polyhedron,
  CGAL::Nef_polyhedron_3<Kernel_3> &p,
  CGAL::Nef_polyhedron_3<Kernel_3> &best,
  double &value,
  double &curr_value,
  double &curr_increase,
  bool &isAdditive,
  fsc::matrix3d &free_space_scores
  )
{
  CGAL::Nef_polyhedron_3<Kernel_3> res = curr_nef_polyhedron + p;
  double f1value = constants::estimation_3d_1_weight * 
    pch::estimation_function_3d_1(res, free_space_scores);
  double f2value = constants::estimation_3d_2_weight * 
    pch::estimation_function_3d_2(res, cloud_ptr);
  double f3value = constants::estimation_3d_3_weight * 
    pch::estimation_function_3d_3(res);
  double new_value = f1value + f2value + f3value;
  mutex.lock();
  if (new_value > curr_value)
  {
    curr_increase = new_value - curr_value;
    value = new_value;
    isAdditive = true;
    best = p;
    std::cout << "curr_increase: " << curr_increase << std::endl;
  }
  mutex.unlock();
  res = curr_nef_polyhedron + p;
  f1value = constants::estimation_3d_1_weight * 
    pch::estimation_function_3d_1(res, free_space_scores);
  f2value = constants::estimation_3d_2_weight * 
    pch::estimation_function_3d_2(res, cloud_ptr);
  f3value = constants::estimation_3d_3_weight * 
    pch::estimation_function_3d_3(res);
  new_value = f1value + f2value + f3value;
  mutex.lock();
  if (new_value > curr_value)
  {
    curr_increase = new_value - curr_value;
    value = new_value;
    isAdditive = true;
    best = p;
    std::cout << "curr_increase: " << curr_increase << std::endl;
  }
  mutex.unlock();
}

/*
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
================================================================================
*/

// void compute_best_polygon_parallel(
//   double interval_idx,
//   double &curr_value, 
//   double &curr_increase,
//   bool &isAdditive, 
//   Traits_2::General_polygon_2 &p, 
//   Traits_2::General_polygon_2 &best, 
//   fsc::matrix3d &free_space_scores, 
//   CGAL::General_polygon_set_2<Traits_2> &curr_polygon_set,
//   std::vector< std::vector<pcl::PointXYZ> > &layers_points)
// {
//   CGAL::General_polygon_set_2<Traits_2> addition_polygon_set(curr_polygon_set);
//   addition_polygon_set.join(p);
//   double f1value = constants::estimation_2d_1_weight * 
//     pch::estimation_function_2d_1(addition_polygon_set, 
//     free_space_scores, interval_idx);
//   double f2value = constants::estimation_2d_2_weight * 
//     pch::estimation_function_2d_2(addition_polygon_set,
//     layers_points[interval_idx], interval_idx);
//   double f3value = constants::estimation_2d_3_weight * 
//     pch::estimation_function_2d_3(addition_polygon_set,
//     layers_points[interval_idx], interval_idx);
//   double value = f1value + f2value + f3value;
//   mutex.lock();
//   if (value - curr_value > curr_increase) 
//   {
//     best = p;
//     isAdditive = true;
//     curr_increase = value - curr_value;
//     if (constants::estimation_2d_print_increase)
//       std::cout << "current increase: " << curr_increase << std::endl;
//   }
//   mutex.unlock();

//   CGAL::General_polygon_set_2<Traits_2> subtraction_polygon_set(curr_polygon_set);
//   subtraction_polygon_set.difference(p);
//   f1value = constants::estimation_2d_1_weight * 
//     pch::estimation_function_2d_1(subtraction_polygon_set, 
//     free_space_scores, interval_idx);
//   f2value = constants::estimation_2d_2_weight * 
//     pch::estimation_function_2d_2(subtraction_polygon_set,
//     layers_points[interval_idx], interval_idx);
//   f3value = constants::estimation_2d_3_weight * 
//     pch::estimation_function_2d_3(subtraction_polygon_set,
//     layers_points[interval_idx], interval_idx);
//   value = f1value + f2value + f3value;
//   mutex.lock();
//   if (value - curr_value > curr_increase) 
//   {
//     best = p;
//     isAdditive = false;
//     curr_increase = value - curr_value;
//     if (constants::estimation_2d_print_increase)
//       std::cout << "current increase: " << curr_increase << std::endl;
//   }
//   mutex.unlock();
// }

/* extract file path of xml, mad and fss files */
int load_data_set_paths() 
{
  std::string line;

  // extract file paths from the text file
  std::ifstream ds_file (DS_FILE_PATH);
  if (not ds_file.is_open())
  {
    std::cerr << "Error!  Unable to open data_set path file: "
       << DS_FILE_PATH << std::endl;
    return -1;
  }

  // read backpack config (.xml)
  if ( not getline (ds_file, backpack_config_file_path) ) 
  {
    std::cerr << "Error!  Unable to read backpack config file path from: "
       << DS_FILE_PATH << std::endl;
    return -1;
  }

  // read mad file's path
  if ( not getline (ds_file,mad_file_path) )
  {
    std::cerr << "Error!  Unable to read backpack config file path from: "
       << DS_FILE_PATH << std::endl;
    return -1;
  }

  // read fss file paths
  while ( getline (ds_file, line)) 
  {
    fss_file_paths.push_back(line); 
  }

  if (fss_file_paths.size() == 0) 
  {
    std::cerr << "Error!  Unable to read fss file path from: "
       << DS_FILE_PATH << std::endl;
    return -1;
  }
  return 0;
}

/* load backpack and scanner position relationship from xml file */
int load_backpack_config()
{
  // read backpack configuration file
  int ret = scanner_path.parse_hardware_config(backpack_config_file_path);
  if(ret)
  {
    std::cerr << "Error!  Unable to open backpack configuration file: "
       << backpack_config_file_path << std::endl;
    return PROPEGATE_ERROR(-1, ret);
  }
  return 0;
}

/* load scanner path from mad file */
int load_scanner_path()
{
  // read mad file, i.e. backpack movement file
  int ret = scanner_path.readmad(mad_file_path);
  if(ret)
  {
    std::cerr << "Error!  Unable to open mad file: "
       << mad_file_path << std::endl;
    return PROPEGATE_ERROR(-1, ret);
  }
  return 0;
}

/* read all points and load them into the point cloud */
int load_data_into_point_cloud()
{
  int ret, num_frames;
  for (std::string file_path: fss_file_paths)
  {
    /* retrieve scanner data */
    infile.set_correct_for_bias(true);
    ret = infile.open(file_path);
    if(ret)
    {
      /* report error */
      std::cerr << "Error!  Unable to open fss file: "
         << file_path << std::endl;
      return -1;
    }

    /* iterate over frames */
    num_frames = infile.num_frames();
    for(int i = 0; i < num_frames; i++)
    {
      /* get frame */
      ret = infile.get(frame, i);
      if (ret)
      {
        /* report error */
        std::cerr << "Error! Can't retrieve frame at time " << i << " for " 
          << infile.scanner_name() << std::endl;
        return -1;
      }

      ret = scanner_path.compute_transform_for(fss_pose,
          frame.timestamp, infile.scanner_name());
      if(ret)
      {
        /* report error */
        std::cerr << "Error! Can't compute fss pose in frame " << i << 
          " at time " << frame.timestamp << " for " << infile.scanner_name() 
          << ", error: " << ret << std::endl;
        return -1;
      }
      for (fss::point_t pt : frame.points)
      {
        Eigen::Vector3d eigen_pt;
        eigen_pt[0] = pt.x;
        eigen_pt[1] = pt.y;
        eigen_pt[2] = pt.z;
        fss_pose.apply(eigen_pt);
        pcl::PointXYZ new_cloud_pt(eigen_pt.x(), eigen_pt.y(), eigen_pt.z());
        cloud_ptr->push_back(new_cloud_pt);
      }
      /* for each frame, record height for each point */
    }
    infile.close();
  }
  std::cout << "size of cloud: " << cloud_ptr->size() << std::endl;
  std::cout << "dimension of cloud: " << cloud_ptr->height << ", " 
    << cloud_ptr->width << std::endl;
  return 0;
}

/* return a vector, v, representing the histogram of point distribution vertically in the space */
std::vector<double> construct_height_histogram() 
{
  int i;
  double ROOM_HEIGHT_UPPER_BOUND, ROOM_HEIGHT_LOWER_BOUND;
  for (pcl::PointXYZ pt : *cloud_ptr)
  {
    if (ROOM_HEIGHT_UPPER_BOUND < pt.z) ROOM_HEIGHT_UPPER_BOUND = pt.z;
    if (ROOM_HEIGHT_LOWER_BOUND < pt.z) ROOM_HEIGHT_LOWER_BOUND = pt.z;
  }
  half_range = fmin(fmax(std::abs(ROOM_HEIGHT_LOWER_BOUND), 
    std::abs(ROOM_HEIGHT_UPPER_BOUND)), 99.0f) + 1;
  std::cout << "height range: " << -half_range << "m to " << half_range 
    << "m" << std::endl;
  std::cout << "number of intervals: " << height_to_interval(half_range) 
    << std::endl;
  std::vector<double> v(height_to_interval(half_range),0);
  for (pcl::PointXYZ pt : *cloud_ptr)
  {
    int idx = height_to_interval(pt.z);
    if (idx > 0 and idx < v.size())
      v[height_to_interval(pt.z)] ++;
  }
  return v;
}

int height_to_interval(double height) {return (height + half_range) 
  / constants::HISTOGRAM_INTERVAL_SIZE;}
int interval_to_lower_bound(int interval) {return interval 
  * constants::HISTOGRAM_INTERVAL_SIZE - half_range;}
int interval_to_upper_bound(int interval) {return (interval+1) 
  * constants::HISTOGRAM_INTERVAL_SIZE - half_range;}
int height_to_free_space_interval(double height) {return (height + half_range) 
  / constants::RESOLUTION;}

void construct_gaussian_smoothing_operator(std::vector<double> &v)
{
  double exponential;
  int center = height_to_interval(half_range)-1;
  for (int i = 0; i <= center; i++)
  {
    exponential = i*0.05/constants::GAUSSIAN_FILTER_STANDARD_DEVIATION;
    exponential *= - exponential;
    v[center - i] = v[center + i] = exp(exponential);
  }
}

void find_peaks(std::vector<double> v, std::vector<int> &peaks)
{
  int i = 0;
  for (i = 0; i < v.size(); i++)
  {
    bool push = true;
    for (int j = fmax(0, i-3); j < fmin(v.size()-1, i+3); j++)
    {
      if (v[i] < v[j])
      {
        push = false;
        break;
      }
    }
    if (push and v[i] > 1)
    {
      peaks.push_back(i); 
    }
  }
}

void filter_histogram(std::vector<double> &filtered_histogram, std::vector<double> 
  histogram, std::vector<double> gaussian_smoothing_filter)
{
  const int center_of_smoothing_opeator = height_to_interval(half_range)-1;
  for (int i = 0; i < histogram.size(); i++)
  {
    for (int j=-i; j < ((int) histogram.size()) - i; j++) // histogram.size() is an unsigned int
    {
      filtered_histogram[i+j] += 
        gaussian_smoothing_filter[center_of_smoothing_opeator + j] * 
        histogram[i];
    }
  }
}

std::vector<double> get_extremum_in_point_vector(
  std::vector< std::vector<pcl::PointXYZ> > layers, 
  std::vector<int> peaks)
{
  double min_x, max_x, min_y, max_y;
  max_x = layers[peaks[peaks.size()-1]][0].x;
  min_x = layers[peaks[peaks.size()-1]][0].x;
  max_y = layers[peaks[peaks.size()-1]][0].y;
  min_y = layers[peaks[peaks.size()-1]][0].y;
  for (double interval_idx : peaks)
  {
    std::vector<pcl::PointXYZ> layer = layers[interval_idx];
    for (pcl::PointXYZ point : layer)
    {
      if (point.x > max_x) max_x = point.x;
      if (point.x < min_x) min_x = point.x;
      if (point.y > max_y) max_y = point.y;
      if (point.y < min_y) min_y = point.y;
    }
  }
  std::vector<double> rtn_vector;
  max_x ++;
  min_x --;
  max_y ++;
  min_y --;
  rtn_vector.push_back(min_x);
  rtn_vector.push_back(min_y);
  rtn_vector.push_back(max_x);
  rtn_vector.push_back(max_y);
  return rtn_vector;
}

void wait()
{  
  std::cin.ignore(1);
}



/* 
precondition: requires a directory "log" to exists before calling
output file std::std::vector< any file with the same name 
*/
template<typename T>
int write_vector_to_file(std::vector<T> v, std::string file_name)
{
  std::string file_path = "./log/" + file_name + ".vector.log";
  remove(file_path.c_str());
  std::ofstream output_file(file_path.c_str());
  for (int i = 0; i < v.size(); i++)
  {
    output_file << i << ": " << v[i] << std::endl;
  }
  output_file.close();
  std::cout << "write to file: " << file_path << std::endl;
  return 0;
}

int write_eigen_VectorXd_to_file(Eigen::VectorXd v, std::string file_name)
{
  std::string file_path = "./log/" + file_name + ".Eigen::VectorXd.log";
  remove(file_path.c_str());
  std::ofstream output_file(file_path.c_str());
  for (int i = 0; i < v.size(); i++)
  {
    output_file << i << ": " << v[i] << std::endl;
  }
  output_file.close();
  std::cout << "write to file: " << file_path << std::endl;
  return 0;
}


template<typename T>
void free_vector(std::vector<T> &v)
{
  v.clear();
  v.shrink_to_fit();
}