#include <pthread.h>
#include <algorithm>
#include <boost/thread.hpp>

/**
 * @file pch.cpp
 * @author Tom Lai <tomlai@.berkeley.edu>
 *
 * @section DESCRIPTION
 *
 * This file contains classes used to manipulate 2d,3d polygons 
 * in furukawa reconstruction algorithm.
 *
 * Make sure to compile with -std=c++11 to support C++11 standard.
 *
 */

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Nef_polyhedron_3.h>
#include <CGAL/Gps_circle_segment_traits_2.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/General_polygon_set_2.h>
#include <CGAL/Lazy_exact_nt.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Polygon_with_holes_2.h>
#include <CGAL/General_polygon_2.h>
#include <CGAL/Polygon_set_2.h>
#include <CGAL/Circle_2.h>
#include <CGAL/Segment_2.h>
#include <CGAL/Bbox_2.h>
#include <CGAL/Polygon_2_algorithms.h>
#include <fsc/fsc.h>
#include <pcl/point_cloud.h>
#include <pch/pch.h>
#include <constants/constants.h>

typedef CGAL::Polygon_2<Kernel_2>::Edge_const_circulator Edge_circulator;

boost::mutex pch_mutex;

using namespace pch;

Traits_2::General_polygon_2 pch::polygonWithLines(fsc::line_2d l1, fsc::line_2d l2,
  fsc::line_2d l3, fsc::line_2d l4)
{
  Traits_2::General_polygon_2 pgn;
  Eigen::Vector2d i;
  i = l1.get_intersection(l3); Kernel_2::Point_2 p1(i[0], i[1]);
  i = l3.get_intersection(l2); Kernel_2::Point_2 p2(i[0], i[1]);
  i = l2.get_intersection(l4); Kernel_2::Point_2 p3(i[0], i[1]);
  i = l4.get_intersection(l1); Kernel_2::Point_2 p4(i[0], i[1]);
  Traits_2::X_monotone_curve_2 s1(p1, p2);    pgn.push_back(s1);
  Traits_2::X_monotone_curve_2 s2(p2, p3);    pgn.push_back(s2);
  Traits_2::X_monotone_curve_2 s3(p3, p4);    pgn.push_back(s3);
  Traits_2::X_monotone_curve_2 s4(p4, p1);    pgn.push_back(s4);
  return pgn;
}

double pch::estimation_function_2d_1(CGAL::General_polygon_set_2<Traits_2> &polygon_set,
    fsc::matrix3d &free_space_scores, double z)
{
  if (constants::estimation_2d_1_weight == 0) return 0;
  double sum = 0; // sum of free space scores inside the polygon
  double total_positive = 0; // total sum in the domain without negative score
  for (int x = 0; x < free_space_scores.x_size(); x++)
  {
    for (int y = 0; y < free_space_scores.y_size(); y++)
    {
      // Traits_2::Polygon_2 voxel_area;
      // voxel_area.push_back(Kernel_2::Point_2(x, y));
      // voxel_area.push_back(Kernel_2::Point_2(x, y+1));
      // voxel_area.push_back(Kernel_2::Point_2(x+1, y+1));
      // voxel_area.push_back(Kernel_2::Point_2(x+1, y));
      double val = free_space_scores.get(x,y,z);
      if (val > 0)
      {
        total_positive += val;
      }
      Traits_2::Point_2 pt(x+0.5, y+0.5);
      if (polygon_set.oriented_side(pt) == CGAL::ON_POSITIVE_SIDE)
      {
        sum += val;
      }
    } 
  }
  double result;
  if (total_positive == 0)
  {
    result = 0;
  }
  else
  {
    result = sum / total_positive;
  }
  if (constants::estimation_2d_1_print_value)
  {
    pch_mutex.lock();
    std::cout  << "2d-1: " << sum << " / " << total_positive << " = " << result << std::endl;
    pch_mutex.unlock();
  }
  return result;
}

// check whether a point is on outer boundary or boundary of the holes
double pch::estimation_function_2d_2(CGAL::General_polygon_set_2<Traits_2> &polygon_set,
    std::vector<pcl::PointXYZ> &points, double z)
{
  if (constants::estimation_2d_2_weight == 0) return 0;
  double sum = 0;
  for (pcl::PointXYZ point : points)
  {
    Traits_2::Point_2 pt(point.x, point.y);
    if (polygon_set.oriented_side(pt) == CGAL::ON_ORIENTED_BOUNDARY)
    {
      sum++;
    }
  }
  double result = sum / points.size();
  if (constants::estimation_2d_2_print_value)
  {
    pch_mutex.lock();
    std::cout  << "2d-2: " << sum << " / " << points.size() << " = " << result << std::endl;
    pch_mutex.unlock();
  }
  return result;
}

double pch::estimation_function_2d_3(CGAL::General_polygon_set_2<Traits_2> &polygon_set,
    std::vector<pcl::PointXYZ> &points, double z)
{
  if (constants::estimation_2d_3_weight == 0) return 0;
  double sum = 0;
  std::vector<Traits_2::Polygon_2> circles;
  for (pcl::PointXYZ point : points)
  {
    Kernel_2::Point_2 pt(point.x, point.y);
    // circle constructor (center, squared radius, orientaion = CC)
    Traits_2::Polygon_2 c = construct_polygon(Kernel_2::Circle_2(pt, 0.04));
    circles.push_back(c);
  }

  std::vector<Traits_2::Polygon_2> segments;

  std::list<Traits_2::General_polygon_with_holes_2> pwhs;
  polygon_set.polygons_with_holes (std::back_inserter (pwhs));

  for (Traits_2::General_polygon_with_holes_2 pwh: pwhs)
  {
    Traits_2::General_polygon_2 outer_boundary = pwh.outer_boundary();
    Traits_2::General_polygon_2::Curve_iterator cur = outer_boundary.curves_begin();
    while (cur != outer_boundary.curves_end())
    {
      Traits_2::General_polygon_2 segment_polygon = construct_segment_polygon(*cur);
      segments.push_back(segment_polygon);
    }

    Traits_2::General_polygon_with_holes_2::Hole_iterator hit = pwh.holes_begin();
    while (hit != pwh.holes_end())
    {
      Traits_2::General_polygon_2 hole = *hit;
      Traits_2::General_polygon_2::Curve_iterator hcur = hole.curves_begin();
      while (hcur != hole.curves_end())
      {
        Traits_2::General_polygon_2 segment_polygon = construct_segment_polygon(*hcur);
        segments.push_back(segment_polygon);
        hcur++;
      }
      hit ++;
    }
  }

  for (Traits_2::Polygon_2 circle : circles)
  {
    for (Traits_2::General_polygon_2 segment : segments)
    {
      if (CGAL::do_intersect(circle, segment))
      {
        sum++;
        break;
      }
    }
  }
  double result = sum / circles.size();
  if (constants::estimation_2d_2_print_value)
  {
    pch_mutex.lock();
    std::cout  << "2d-3: " << sum << " / " << points.size() << " = " << result << std::endl;
    pch_mutex.unlock();
  }
  return result;
}

// prune polygons by the 3 criteria
void pch::prune(std::vector<Traits_2::General_polygon_2> &polygons)
{
  std::set<int> idxToPruneSet;
  std::vector<int> idxToPruneList;

  // case 1, edge length < 0.15 meter 
  for (int i = 0; i < polygons.size(); i++)
  {
    Traits_2::General_polygon_2::Curve_iterator cur = polygons[i].curves_begin();
    while (cur != polygons[i].curves_end())
    {
      Traits_2::X_monotone_curve_2 edge = *cur;
      if (CGAL::to_double(length(edge)) < constants::prune_dimension_lower_bound)
      {
        // std::cout << "prune polygon " << i << " with edge: " << edge << " with length: " << CGAL::to_double(length(edge)) << std::endl;
        // std::cout << polygons[i] << std::endl;
        idxToPruneSet.insert(i);
        break;
      }
      ++cur;
    }
  }

  idxToPruneList = std::vector<int>(idxToPruneSet.begin(),idxToPruneSet.end()); 
  std::sort(idxToPruneList.begin(), idxToPruneList.end(), std::greater<int>());
  for (int idx : idxToPruneList)
  {
    polygons.erase(polygons.begin()+idx);
  }
  std::cout << "prune function 1: pruned " << idxToPruneList.size() << " polygon\n";
  idxToPruneSet.clear();

  // case 2, distance between centers < 0.3, difference between dimension < 0.2
  // weird exception occurs here with target being empty in some circumstances
  for (int i = 0; i < polygons.size(); i++)
  {
    Traits_2::General_polygon_2 polygon1 = polygons[i];
    pcl::PointXYZ center1 = center(polygon1);
    for (int j = i+1; j < polygons.size(); j++)
    {
      Traits_2::General_polygon_2 polygon2 = polygons[j];
      pcl::PointXYZ center2 = center(polygon2);
      if (sqrt(pow(center1.x - center2.x,2) + pow(center1.y - center2.y,2)) < 
        constants::prune_distance_between_centers / constants::RESOLUTION)
      {
        Traits_2::General_polygon_2::Curve_iterator cur = polygons[i].curves_begin();
        Traits_2::X_monotone_curve_2 polygon1edge1 = *cur;
        // std::cout << "source: " << polygon1edge1.source() << std::endl;
        // std::cout << "target: " << polygon1edge1.target() << std::endl;
        ++cur;
        Traits_2::X_monotone_curve_2 polygon1edge2 = *cur;

        cur = polygons[j].curves_begin();
        Traits_2::X_monotone_curve_2 polygon2edge1 = *cur;
        ++cur;
        Traits_2::X_monotone_curve_2 polygon2edge2 = *cur;
        fsc::line_2d p1l1(
          to_double(polygon1edge1.source().x()), 
          to_double(polygon1edge1.source().y()),
          to_double(polygon1edge1.target().x()),
          to_double(polygon1edge1.target().y()));
        fsc::line_2d p1l2(
          to_double(polygon1edge2.source().x()), 
          to_double(polygon1edge2.source().y()),
          to_double(polygon1edge2.target().x()), 
          to_double(polygon1edge2.target().y()));
        fsc::line_2d p2l1(
          to_double(polygon2edge1.source().x()), 
          to_double(polygon2edge1.source().y()), 
          to_double(polygon2edge1.target().x()), 
          to_double(polygon2edge1.target().y()));
        fsc::line_2d p2l2(
          to_double(polygon2edge2.source().x()), 
          to_double(polygon2edge2.source().y()), 
          to_double(polygon2edge2.target().x()), 
          to_double(polygon2edge2.target().y()));
        if ((p1l1.is_parallel_to(p2l1)
          and abs(p1l1.length() - p2l1.length()) < 
            constants::prune_difference_between_dimensions/constants::RESOLUTION
          and abs(p1l2.length() - p2l2.length()) < 
            constants::prune_difference_between_dimensions/constants::RESOLUTION) 
          or
          (p1l1.is_parallel_to(p2l2)
          and abs(p1l1.length() - p2l2.length()) < 
            constants::prune_difference_between_dimensions/constants::RESOLUTION 
          and abs(p1l2.length() - p2l1.length()) < 
            constants::prune_difference_between_dimensions/constants::RESOLUTION))
        {
          idxToPruneSet.insert(j);
        }
      }
    }
  }

  idxToPruneList = std::vector<int>(idxToPruneSet.begin(),idxToPruneSet.end()); 
  std::sort(idxToPruneList.begin(), idxToPruneList.end(), std::greater<int>());
  for (int idx : idxToPruneList)
  {
    polygons.erase(polygons.begin()+idx);
  }
  std::cout << "prune function 2: pruned " << idxToPruneList.size() << " polygons\n";

  // case 3, points within 0.2 meter from boundary < 0.05 * total points
}

pcl::PointXYZ pch::center(Traits_2::General_polygon_2 &polygon)
{
  CGAL::Bbox_2 box = polygon.bbox();
  pcl::PointXYZ pt;
  pt.x = 0.5 * (box.xmin() + box.xmax());
  pt.y = 0.5 * (box.ymin() + box.ymax());
  return pt;
}

// // Construct a polygon from a circle.
Traits_2::Polygon_2 construct_polygon(const Kernel_2::Circle_2& circle)
{
  // Subdivide the circle into two x-monotone arcs.
  Traits_2 traits;
  Traits_2::Curve_2 curve (circle);
  std::list<CGAL::Object>  objects;
  traits.make_x_monotone_2_object() (curve, std::back_inserter(objects));
  CGAL_assertion (objects.size() == 2);
  // Construct the polygon.
  Traits_2::General_polygon_2  pgn;
  Traits_2::X_monotone_curve_2 arc;
  std::list<CGAL::Object>::iterator iter;
  for (iter = objects.begin(); iter != objects.end(); ++iter) {
    CGAL::assign (arc, *iter);
    pgn.push_back (arc);
  }
  return pgn;
}

// trait::General_polygon_2  construct_segment_polygon (CGAL::Segment_2<Kernel_2> edge)
// {
//   trait::General_polygon_2 pgn;
//   trait::X_monotone_curve_2 s1(edge.source(), edge.target());    
//   pgn.push_back(s1);
//   return pgn;
// }

Traits_2::General_polygon_2  construct_segment_polygon (Traits_2::X_monotone_curve_2 s1)
{
  Kernel_2::Point_2 sp = Kernel_2::Point_2(CGAL::to_double(s1.target().x()), CGAL::to_double(s1.target().y()));
  Kernel_2::Point_2 ep = Kernel_2::Point_2(CGAL::to_double(s1.source().x()), CGAL::to_double(s1.source().y()));
  Traits_2::X_monotone_curve_2 s2(sp, ep);

  Traits_2::General_polygon_2 pgn;  
  pgn.push_back(s1);
  pgn.push_back(s2);
  return pgn;
}

// trait::General_polygon_2  construct_segment_polygon (const Kernel_2::Point_2& p1, 
//   const Kernel_2::Point_2& p2)
// {
//   trait::General_polygon_2 pgn;
//   trait::X_monotone_curve_2 s1(p1, p2);    
//   pgn.push_back(s1);
//   return pgn;
// }

// void construct_active_area_parallel(
//   int thread_id, 
//   std::vector<pcl::PointXYZ> &points, 
//   CGAL::General_polygon_set_2<trait> &active_area)
// {
//   CGAL::General_polygon_set_2<trait> partial_active_area;
//   for (int i = thread_id; i < points.size(); i+=constants::NUM_THREADS)
//   {
//     CGAL::Point_2<Kernel_2> pt(points[i].x, points[i].y);
//     trait::General_polygon_2 circle = construct_polygon(Kernel_2::Circle_2(pt, 0.04));
//     partial_active_area.join(circle);
//   }
//   pch_mutex.lock();
//   active_area.join(partial_active_area);
//   pch_mutex.unlock();
// }

double pch::length(Traits_2::X_monotone_curve_2 line)
{
  Traits_2::Point_2 start = line.source();
  double x1 = CGAL::to_double(start.x());
  double y1 = CGAL::to_double(start.y());
  Traits_2::Point_2 end = line.target();
  double x2 = CGAL::to_double(end.x());
  double y2 = CGAL::to_double(end.y());
  return sqrt(pow(x1 - x2,2) + pow(y1 - y2,2));
}


// cube construction code borrowed from 
// http://doc.cgal.org/latest/Polyhedron/Polyhedron_2polyhedron_prog_cube_8cpp-example.html
bool pch::make_cube_3(CGAL::Polyhedron_3<Kernel_3> &out, Traits_2::General_polygon_2 &p,
    double h1, double h2)
{
  std::vector<Traits_2::Point_2> v;
  for (Traits_2::General_polygon_2::Curve_iterator vi = p.curves_begin(); vi != p.curves_end(); ++vi)
  {
    for (Traits_2::Point_2 pt : v)
    {
      if (pt.x() == vi->source().x() and pt.y() == vi->source().y())
      {
        std::cout << "failed to build polygon with \n" << p << std::endl;
        std::cout << "for point" << pt << std::endl;
        return false;
      }
    }
    v.push_back(vi->source());
  }

  if (v.size() != 4 or h1 >= h2) 
  {
    std::cout << "failed to build polygon with \n" << p << std::endl;
    return false;
  }

  // appends a cube of size [0,1]^3 to the polyhedron out.
  CGAL::Polyhedron_3<Kernel_3>::Halfedge_handle h = out.make_tetrahedron( 
                                          Kernel_3::Point_3( CGAL::to_double(v[1].x()), CGAL::to_double(v[1].y()), h1),
                                          Kernel_3::Point_3( CGAL::to_double(v[0].x()), CGAL::to_double(v[0].y()), h2),
                                          Kernel_3::Point_3( CGAL::to_double(v[0].x()), CGAL::to_double(v[0].y()), h1),
                                          Kernel_3::Point_3( CGAL::to_double(v[3].x()), CGAL::to_double(v[3].y()), h1));
  CGAL::Polyhedron_3<Kernel_3>::Halfedge_handle g = h->next()->opposite()->next();
  out.split_edge( h->next());
  out.split_edge( g->next());
  out.split_edge( g);
  h->next()->vertex()->point()     = Kernel_3::Point_3( CGAL::to_double(v[1].x()), CGAL::to_double(v[1].y()), h2);
  g->next()->vertex()->point()     = Kernel_3::Point_3( CGAL::to_double(v[3].x()), CGAL::to_double(v[3].y()), h2);
  g->opposite()->vertex()->point() = Kernel_3::Point_3( CGAL::to_double(v[2].x()), CGAL::to_double(v[2].y()), h1);
  CGAL::Polyhedron_3<Kernel_3>::Halfedge_handle f = out.split_facet( g->next(),
                                     g->next()->next()->next());
  CGAL::Polyhedron_3<Kernel_3>::Halfedge_handle e = out.split_edge( f);
  e->vertex()->point() = Kernel_3::Point_3( CGAL::to_double(v[2].x()), CGAL::to_double(v[2].y()), h2);
  out.split_facet( e, f->next()->next());

  // the below piece of code will not work because the cube constructed is degenerated 
  // an alternative way is to construct 5 pieces of tetrahedron nef polyhedron,
  // fuse them then re-create the polyhedron 
  // std::vector<Kernel_3::Point_3> v1;
  // v1.push_back(Kernel_3::Point_3( CGAL::to_double(v[0].x()), CGAL::to_double(v[0].y()), h1));
  // v1.push_back(Kernel_3::Point_3( CGAL::to_double(v[1].x()), CGAL::to_double(v[1].y()), h1));
  // v1.push_back(Kernel_3::Point_3( CGAL::to_double(v[2].x()), CGAL::to_double(v[2].y()), h1));
  // v1.push_back(Kernel_3::Point_3( CGAL::to_double(v[3].x()), CGAL::to_double(v[3].y()), h1));
  // v1.push_back(Kernel_3::Point_3( CGAL::to_double(v[0].x()), CGAL::to_double(v[0].y()), h2));
  // v1.push_back(Kernel_3::Point_3( CGAL::to_double(v[1].x()), CGAL::to_double(v[1].y()), h2));
  // v1.push_back(Kernel_3::Point_3( CGAL::to_double(v[2].x()), CGAL::to_double(v[2].y()), h2));
  // v1.push_back(Kernel_3::Point_3( CGAL::to_double(v[3].x()), CGAL::to_double(v[3].y()), h2));
  // out.make_tetrahedron(v1[0],v1[4],v1[5],v1[7]);
  // out.make_tetrahedron(v1[7],v1[0],v1[3],v1[2]);
  // out.make_tetrahedron(v1[2],v1[0],v1[5],v1[7]);
  // out.make_tetrahedron(v1[7],v1[6],v1[5],v1[2]);
  // out.make_tetrahedron(v1[2],v1[1],v1[0],v1[5]);
  return true;
}

// include boundaries
bool pch::point_inside_nef_polyhedron(Kernel_3::Point_3& p, 
  CGAL::Nef_polyhedron_3<Kernel_3> N)
{
  CGAL::Nef_polyhedron_3<Kernel_3>::Volume_const_handle cc;
  CGAL::Nef_polyhedron_3<Kernel_3>::Object_handle o = N.locate(p);
  if (CGAL::assign(cc,o) and (*cc).mark()==0)
    return false;
  return true;
}

bool pch::point_on_boundary_of_nef_polyhedron(Kernel_3::Point_3& p, 
    CGAL::Nef_polyhedron_3<Kernel_3> N)
{
  CGAL::Nef_polyhedron_3<Kernel_3>::Volume_const_handle cc;
  CGAL::Nef_polyhedron_3<Kernel_3>::Object_handle o = N.locate(p);
  if (CGAL::assign(cc,o))
    return false;
  return true;
}

// 3d level estimation function 1
// sum of free space scores inside T / total sum of positive voxels in domain
double pch::estimation_function_3d_1(CGAL::Nef_polyhedron_3<Kernel_3> N,
  fsc::matrix3d &free_space_scores)
{
  if (constants::estimation_3d_1_weight == 0) return 0;
  double sum = 0; // sum of free space scores inside the polygon
  double total_positive = 0; // total sum in the domain without negative score
  for (int x = 0; x < free_space_scores.x_size(); x++)
  {
    for (int y = 0; y < free_space_scores.y_size(); y++)
    {
      for (int z = 0; z < free_space_scores.z_size(); z++)
      {
        double val = free_space_scores.get(x,y,z);
        if (val > 0)
        {
          total_positive += val;
        }
        Kernel_3::Point_3 point(x,y,z);
        if (point_inside_nef_polyhedron(point,N))
        {
          sum += val;
        }
      } 
    } 
  }
  double result;
  if (total_positive == 0)
  {
    result = 0;
  }
  else
  {
    result = sum / total_positive;
  }
  if (constants::estimation_3d_1_print_value)
  {
    pch_mutex.lock();
    std::cout  << "3d-1: " << sum << " / " << total_positive << " = " << result << std::endl;
    pch_mutex.unlock();
  }
  return result;
}

// 3d level estimation function 2
// # of points on the surface of T / total # of points
double pch::estimation_function_3d_2(CGAL::Nef_polyhedron_3<Kernel_3> N,
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ptr)
{
  if (constants::estimation_3d_2_weight == 0) return 0;
  double sum = 0;
  for (pcl::PointXYZ point : *cloud_ptr)
  {
    Kernel_3::Point_3 pt(point.x, point.y, point.z);
    if (point_on_boundary_of_nef_polyhedron(pt, N))
    {
      sum++;
    }
  }
  double result = sum / (*cloud_ptr).width;
  if (constants::estimation_3d_2_print_value)
  {
    pch_mutex.lock();
    std::cout  << "3d-2: " << sum << " / " << (*cloud_ptr).width << " = " << result << std::endl;
    pch_mutex.unlock();
  }
  return result;
}



double pch::estimation_function_3d_3(CGAL::Nef_polyhedron_3<Kernel_3> N)
{
  if (constants::estimation_3d_3_weight == 0) return 0;
  return 0;
}









