#ifndef PCH_TL_H
#define PCH_TL_H

/**
 * @file pch.h
 * @author Tom Lai <tomlai@.berkeley.edu>
 *
 * @section DESCRIPTION
 *
 * This file contains classes used to manipulate 2d,3d polygons 
 * in furukawa reconstruction algorithm.
 *
 * Make sure to compile with -std=c++11 to support C++11 standard.
 *
 */

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Nef_polyhedron_3.h>
#include <CGAL/Gps_circle_segment_traits_2.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Polygon_with_holes_2.h>
#include <CGAL/Polygon_set_2.h>
#include <fsc/fsc.h>
#include <pcl/point_cloud.h>

typedef CGAL::Epeck Kernel_2; // 2d kernel
typedef CGAL::Epeck Kernel_3; // 3d kernel
typedef CGAL::Gps_circle_segment_traits_2<Kernel_2> Traits_2;

namespace pch
{
  // construct polygon with the given 4 lines where
  // l1, l2 are parallel edges
  // l3, l4 are the opposite parallel edges
  Traits_2::General_polygon_2 polygonWithLines(fsc::line_2d l1, fsc::line_2d l2,
  fsc::line_2d l3, fsc::line_2d l4);

  // 2d level estimation function 1
  // sum of free space scores inside T / total sum of positive voxels in domain
  double estimation_function_2d_1(CGAL::General_polygon_set_2<Traits_2> &polygon_set,
    fsc::matrix3d &free_space_scores, double z);

  // 2d level estimation function 2
  // # of points on the surface of T / total # of points
  double estimation_function_2d_2(CGAL::General_polygon_set_2<Traits_2> &polygon_set,
    std::vector<pcl::PointXYZ> &points, double z);

  // 2d level estimation function 3
  // number of points within 0.5m to boundary of T / total # of points
  double estimation_function_2d_3(CGAL::General_polygon_set_2<Traits_2> &polygon_set,
    std::vector<pcl::PointXYZ> &points, double z);

  // 2d pruning
  void prune(std::vector<Traits_2::General_polygon_2> &polygons);

  pcl::PointXYZ center(Traits_2::General_polygon_2 &polygon);

  double length(Traits_2::X_monotone_curve_2 line);

  // construct a cube and append it to out
  bool make_cube_3(CGAL::Polyhedron_3<Kernel_3> &out, 
    Traits_2::General_polygon_2 &P, double h1, double h2);

  bool point_inside_nef_polyhedron(Kernel_3::Point_3& p, 
    CGAL::Nef_polyhedron_3<Kernel_3> N);

  bool point_on_boundary_of_nef_polyhedron(Kernel_3::Point_3& p, 
    CGAL::Nef_polyhedron_3<Kernel_3> N);

  // 3d level estimation function 1
  // sum of free space scores inside T / total sum of positive voxels in domain
  double estimation_function_3d_1(CGAL::Nef_polyhedron_3<Kernel_3> N,
    fsc::matrix3d &free_space_scores);

  // 3d level estimation function 2
  // # of points on the surface of T / total # of points
  double estimation_function_3d_2(CGAL::Nef_polyhedron_3<Kernel_3> N,
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ptr);

  // 3d level estimation function 3
  // number of points within 0.5m to surface of T
  // NOT GOING TO IMPLEMENT FOR NOW
  double estimation_function_3d_3(CGAL::Nef_polyhedron_3<Kernel_3> N);
}
Traits_2::Polygon_2 construct_polygon(const Kernel_2::Circle_2& circle);
Traits_2::General_polygon_2  construct_segment_polygon (Traits_2::X_monotone_curve_2 s1);
Traits_2::General_polygon_2  construct_segment_polygon (CGAL::Segment_2<Kernel_2> edge);
Traits_2::General_polygon_2  construct_segment_polygon (const Kernel_2::Point_2& p1, 
  const Kernel_2::Point_2& p2);

#endif