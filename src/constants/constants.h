#ifndef FAC_TL_H
#define FAC_TL_H

/**
 * @file fsc.h
 * @author Tom Lai <tomlai@.berkeley.edu>
 *
 * @section DESCRIPTION
 *
 * This file defines all the constants used in the algorithm
 * 
 */

namespace constants
{
  // number of threads to used in multithreading
  const int NUM_THREADS = 8;

  // the size of each 3-D voxel
  // must be same as HISTOGRAM_INTERVAL_SIZE
  const double RESOLUTION = 0.2;

  // the standard deviation to use in gaussian smoothening for the height
  // histogram
  const double GAUSSIAN_FILTER_STANDARD_DEVIATION = 0.1; 

  // the height of each z-layer
  // must be same as RESOLUTION
  const double HISTOGRAM_INTERVAL_SIZE = RESOLUTION;

  // the evidence threshold for including a line into the final results of 
  // hough transformation
  const int HOUGH_TRANSFORM_THRESHOLD = 10;

  // draw the result of hough transformation on extra window
  const bool DEBUGGING_DISPLAY_HOUGH_TRANSFORM_LINES = true;

  // the maximum value for the free space score in each voxel
  const double VOXEL_MAX_VALUE = 15;

  // pruning condition 1, prune rectangles with dimension length
  const double prune_dimension_lower_bound = 0.2;

  // pruning condition 2, prune rectangles with center distance
  const double prune_distance_between_centers = 0.3;

  // pruning condition 2, prune rectangles with dimension difference
  const double prune_difference_between_dimensions = 0.2;

  // the negative multiplier for zero free space score voxels
  const double NEGATIVE_VOXEL_MULTIPLIER = -30;

  const bool estimation_2d_print_increase = true;

  // the weight for estimation function 1 in 2D layer
  // 0.0 disables this estimation function
  const double estimation_2d_1_weight = 1.0;
  const bool estimation_2d_1_print_value = false;

  // the weight for estimation function 2 in 2D layer
  // 0.0 disables this estimation function
  const double estimation_2d_2_weight = 0.75;
  const bool estimation_2d_2_print_value = false;
  
  // the weight for estimation function 3 in 2D layer
  // 0.0 disables this estimation function
  const double estimation_2d_3_weight = 0.0;
  const bool estimation_2d_3_print_value = false;

  const double estimation_2d_pursue_threshold_value = 0.05;

  const bool estimation_3d_print_final_value = true;

  // the weight for estimation function 1 in 3D layer
  // 0.0 disables this estimation function
  const double estimation_3d_1_weight = 1.0;
  const bool estimation_3d_1_print_value = true;

  // the weight for estimation function 2 in 3D layer
  // 0.0 disables this estimation function
  const double estimation_3d_2_weight = 0.75;
  const bool estimation_3d_2_print_value = true;
  
  // the weight for estimation function 3 in 3D layer
  // 0.0 disables this estimation function
  const double estimation_3d_3_weight = 0.0;
  const bool estimation_3d_3_print_value = false;

  const double estimation_3d_pursue_threshold_value = 0.05;
}

#endif
