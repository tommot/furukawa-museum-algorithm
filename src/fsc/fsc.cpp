#include "fsc.h"
#include <string>
#include <iostream>
#include <cmath>
#include <pthread.h>
#include <algorithm>
#include <mutex>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl/point_types.h>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <constants/constants.h>

/**
 * @file fsc.cpp
 * @author Tom Lai <tomlai@.berkeley.edu>
 *
 * @section DESCRIPTION
 *
 * This file contains classes used to manipulate and compute free space score 
 * in furukawa reconstruction algorithm.
 *
 * Make sure to compile with -std=c++11 to support C++11 standard.
 * 
 * Requires Eigen3 and PCL library to compile correctly.
 *
 */

using namespace fsc;
using namespace pcl;

pthread_mutex_t fsc_mutex = PTHREAD_MUTEX_INITIALIZER;

typedef struct {
  int thread_id;
  double factor;
  octree::OctreePointCloudSearch<PointXYZ> *octree;
  matrix3d *m3;
} parallel_negative_score_computation_input_struct;

void* matrix3d::compute_negative_free_space_score_parallel(void *inputs)
{
  parallel_negative_score_computation_input_struct *input = 
    (parallel_negative_score_computation_input_struct *)inputs;
  int z = input->thread_id;
  Eigen::MatrixXd m(input->m3->get_z_layer(z));
  for (int x = 0; x < input->m3->x_size(); x++)
  {
    for (int y = 0; y < input->m3->y_size(); y++)
    {
      if (m(x,y) == 0) 
      {
        std::vector<int> nearestPointsIndices;
        std::vector<float> pointsSquaredDistances;
        int idx;
        float sqr_distance;
        PointXYZ searchPoint = PointXYZ(x+0.5, y+0.5, z+0.5);
        input->octree->approxNearestSearch (searchPoint, idx, sqr_distance);
        m(x,y) = sqrt(sqr_distance) * input->factor;
      }
    }
  }
  pthread_mutex_lock( &fsc_mutex );
  input->m3->set_z_layer(z, m);
  pthread_mutex_unlock( &fsc_mutex );
  return NULL;
}

line_2d::line_2d(double x1, double y1, double x2, double y2)
{
  Eigen::Vector2d point;
  point[0] = x1;
  point[1] = y1;
  Eigen::Vector2d displacement;
  displacement[0] = x2-x1;
  displacement[1] = y2-y1;
  this->point = point;
  this->displacement = displacement;
}

line_2d::line_2d(Eigen::Vector2d point, Eigen::Vector2d displacement)
{
  this->point = point;
  this->displacement = displacement;
}

line_2d line_2d::perpendicular_line()
{
  double slope = this->slope();
  Eigen::Vector2d d;
  if (slope == std::numeric_limits<double>::infinity())
  {
    d[0] = 1;
    d[1] = 0;
  }
  else if (slope == 0)
  {
    d[0] = 0;
    d[1] = 1;
  }
  else
  {
    d[0] = this->displacement[1];
    d[1] = -this->displacement[0];
  }
  return line_2d::line_2d(this->point, d);
}

bool line_2d::has_intersection(line_2d line)
{
  if (this->is_parallel_to(line)) return false;
  // there must be an _intersection so line.slope() must not be +inf here
  if (this->slope() == std::numeric_limits<double>::infinity())
  {
    // y_1 = m_1 x_0 + y_1 - m_1 x_1
    double t = (this->point[0] - line.point[0]) / line.slope();
    return (in_range(t, 0, 1))? true: false;
  }
  else if (line.slope() == std::numeric_limits<double>::infinity())
  {
    // y_0 = m_0 x_1 + y_0 - m_0 x_0
    double t = (line.point[0] - this->point[0]) / this->slope();
    return (in_range(t, 0, 1))? true: false;
  }
  else
  {
    // x = (y2-y1)+m1x1-m2x2 / m1-m2
    double pos = (line.point[1] - this->point[1] + this->slope() * this->point[0] - line.slope()
      * line.point[0]) / (this->slope() - line.slope());
    double t1 = (pos - this->point[0]) / this->slope();
    double t2 = (pos - this->point[0]) / this->slope();
    return (in_range(t1, 0, 1) and in_range(t2, 0, 1))? true: false;
  }
  return true;
}

Eigen::Vector2d line_2d::get_intersection(line_2d line)
{
  if (not this->is_parallel_to(line))
  {
    return Eigen::Vector2d();
  }
  Eigen::Vector2d pt;
  double slope1 = this->slope();
  double slope2 = line.slope();
  // there must be an _intersection so slope2 must not be +inf here
  if (slope1 == std::numeric_limits<double>::infinity())
  {
    // y_1 = m_1 x_0 + y_1 - m_1 x_1
    pt[0] = this->point[0];
    pt[1] = slope2 * pt[0] + line.point[1] - slope2 * line.point[0];
  }
  else if (slope2 == std::numeric_limits<double>::infinity())
  {
    // y_0 = m_0 x_1 + y_0 - m_0 x_0
    pt[0] = line.point[0];
    pt[1] = slope1 * pt[0] + this->point[1] - slope1 * this->point[0];
  }
  else
  {
    // x = (y2-y1)+m1x1-m2x2 / m1-m2
    pt[0] = (line.point[1] - this->point[1] + slope1 * this->point[0] - slope2
      * line.point[0]) / (slope1 - slope2);
    // y = m1x+c1, c1 = y1 - m1x1
    pt[1] = slope1 * pt[0] + this->point[1] - slope1 * this->point[0];
  }
  return pt;
}

bool line_2d::is_parallel_to(line_2d line)
{
  return abs(this->degree() - line.degree()) <= degree_of_tolerance;
}

bool line_2d::is_perpendicular_to(line_2d line)
{
  return abs(90.0 - abs(this->degree() - line.degree())) <= degree_of_tolerance;
}

std::string line_2d::toString() const 
{
  return "fsc::line_2d {(" + std::to_string(this->point[0]) + ", " + std::to_string(this->point[1]) + ") -> ("  + std::to_string(this->point[0] + this->displacement[0]) + ", " + std::to_string(this->point[1] + this->displacement[1]) + ")}";
}

// bool fsc::operator== (line_2d &l1, line_2d &l2)
// {
//   return l1.point == l2.point and l1.displacement == l2.displacement;
// }

line_3d::line_3d(Eigen::Vector3d point, Eigen::Vector3d displacement)
{
  this->point = point;
  this->displacement = displacement;
}

std::string line_3d::toString() const 
{
  return "(" + std::to_string(this->point[0]) + ", " + std::to_string(this->point[1]) + ", " + std::to_string(this->point[2]) + ") -> ("  + std::to_string(this->point[0] + this->displacement[0]) + ", " + std::to_string(this->point[1] + this->displacement[1]) + ", " + std::to_string(this->point[2] + this->displacement[2]) + ")";
}

matrix3d::matrix3d(int x, int y, int z)
{
  printf("3d matrix size: (%d, %d, %d)\n",x,y,z);
  this->x = x;
  this->y = y;
  this->z = z;
  for (int i = 0; i < z; i++)
  {
    Eigen::MatrixXd m(x,y);
    this->matrix.push_back(m);
  }
}

// return number of columns in x-dimension
int matrix3d::x_size() const
{
  return this->x;
}

// return number of columns in y-dimension
int matrix3d::y_size() const
{
  return this->y;
}

// return number of columns in z-dimension
int matrix3d::z_size() const
{
  return this->z;
}

// Compute and increment the free space score for appropriate voxels for the 
// given line
void matrix3d::increment_for_line(line_3d line)
{
  // std::cout << line << std::endl;
  std::vector<double> t_intersections;
  double x = line.point[0];
  double y = line.point[1];
  double z = line.point[2];
  double dx = line.displacement[0];
  double dy = line.displacement[1];
  double dz = line.displacement[2];

  if (fabs(x) > 1000 or fabs(y) > 1000 or fabs(z) > 1000 or fabs(dx) > 1000 or 
    fabs(dy) > 1000 or fabs(dz) > 1000) 
    {
      // std::cout << "done with line\n";
      return;
    }
  if (dx > 0)
  {
    double max_x = fmin(x+dx, this->x_size());
    for (int i = ceil(x); i <= max_x; i++)
    {
      t_intersections.push_back(((double)i-x)/dx);
    }
  }
  else if (dx < 0)
  {
    double min_x = fmax(x+dx, 0);
    for (int i = floor(x); i >= min_x; i--)
    {
      t_intersections.push_back((i-x)/dx);
    }
  }

  if (dy > 0)
  {
    double max_y = fmin(y+dy, this->y_size());
    for (int j = ceil(y); j <= max_y; j++)
    {
      t_intersections.push_back((j-y)/dy);
    }
  }
  else if (dy < 0)
  {
    double min_y = fmax(y+dy, 0);
    for (int j = floor(y); j >= min_y; j--)
    {
      t_intersections.push_back((j-y)/dy);
    }
  }

  if (dz > 0)
  {
    double max_z = fmin(z+dz, this->z_size());
    for (int k = floor(z); k <= max_z; k++)
    {
      t_intersections.push_back((k-z)/dz);
    }
  }
  else if (dz < 0)
  {
    double min_z = fmax(z+dz, 0);
    for (int k = floor(z); k >= min_z; k--)
    {
      t_intersections.push_back((k-z)/dz);
    }
  }
  if (t_intersections.size() == 0) return;
  std::sort(t_intersections.begin(), t_intersections.end());
  double prev_x,prev_y,prev_z,curr_x,curr_y,curr_z;
  prev_x = floor(x);
  prev_y = floor(y);
  prev_z = floor(z);
  this->increment(prev_x, prev_y, prev_z);
  for (double t : t_intersections)
  {
    curr_x = floor(x+t*dx); 
    curr_y = floor(y+t*dy);
    curr_z = floor(z+t*dz);
    if ((prev_x != curr_x or prev_y != curr_y or prev_z != curr_z) and 
      in_range(curr_x, 0, this->x_size()-1) and 
      in_range(curr_y, 0, this->y_size()-1) and 
      in_range(curr_z, 0, this->z_size()-1))
    {
      if (this->get(curr_x,curr_y,curr_z) < constants::VOXEL_MAX_VALUE) 
        this->increment(curr_x, curr_y, curr_z);
      prev_x = curr_x;
      prev_y = curr_y;
      prev_z = curr_z;
    }
  }
  // std::cout << "done with line\n";
}

// enforce max value on every entry in the 3d matrix
void matrix3d::enforce_max_value(int value)
{
  int count = 0;
  for (int x = 0; x < x_size(); x++)
  {
    for (int y = 0; y < y_size(); y++)
    {
      for (int z = 0; z < z_size(); z++)
      {
        if (this->get(x,y,z) > value) 
        {
          this->set(x,y,z,value);
          count++;
        }
      }
    }
  }
  std::cout << "enforce max value on " << count << " voxels" << std::endl;
}

/* this function constructs an octree of the matrix's positive count voxel and 
 * start search from non-negative voxel, distance between center of voxels is 
 * used to compute the distance between voxels
 * note that pointCloud::Ptr is a boost::shared_ptr, delete will be called 
 * automatically
 */
void matrix3d::compute_negative_free_space_score(double resolution, 
  double factor)
{
  int zero_voxel_count = 0;
  PointCloud<PointXYZ>::Ptr cloud_ptr(new PointCloud<PointXYZ>);
  for (int x = 0; x < x_size(); x++)
  {
    for (int y = 0; y < y_size(); y++)
    {
      for (int z = 0; z < z_size(); z++)
      {
        if (this->get(x,y,z) >= 1) 
        {
          cloud_ptr->push_back(PointXYZ(x+0.5, y+0.5, z+0.5));
        }
        else
        {
          zero_voxel_count++;
        }
      }
    }
  }

  octree::OctreePointCloudSearch<PointXYZ> octree (resolution);
  octree.setInputCloud (cloud_ptr);
  octree.addPointsFromInputCloud ();

  int NUM_THREADS = z_size();
  pthread_t threads[NUM_THREADS];

  for(int i=0; i < NUM_THREADS; i++)
   {
    parallel_negative_score_computation_input_struct input;
    input.thread_id = i;
    input.factor = factor;
    input.octree = &octree;
    input.m3 = this;
    pthread_create( &threads[i], NULL, 
      compute_negative_free_space_score_parallel, &input);
   }

   for(int i=0; i < NUM_THREADS; i++)
   {
      pthread_join( threads[i], NULL);
   }


  // for (int z = 0; z < z_size(); z++)
  // {
  //   parallel_input_struct input;
  //   workers.push_back(std::thread(
  //     &matrix3d::compute_negative_free_space_score_parallel, 
  //     *this, 
  //     z, 
  //     factor, 
  //     std::ref(octree)));
  // }

  

  // std::for_each(workers.begin(), workers.end(), [](std::thread &t) 
  // {
  //   if (t.joinable())
  //   {
  //     t.join();
  //   }
  // });

  // int count = 0;
  // for (int z = 0; z < z_size(); z++)
  // {
  //   for (int x = 0; x < x_size(); x++)
  //   {
  //     for (int y = 0; y < y_size(); y++)
  //     {
  //       count ++;
  //       if (this->get(x,y,z) == 0) 
  //       {
  //         int idx;
  //         float sqr_distance;
  //         PointXYZ searchPoint = PointXYZ(x+0.5, y+0.5, z+0.5);
  //         octree.approxNearestSearch (searchPoint, idx, sqr_distance);
  //         count ++;
  //         this->set(x,y,z,sqrt(sqr_distance) * factor);
  //       }
  //     }
  //   }
  // }

  int zero_voxel_count2 = 0;
  for (int x = 0; x < x_size(); x++)
  {
    for (int y = 0; y < y_size(); y++)
    {
      for (int z = 0; z < z_size(); z++)
      {
        if (this->get(x,y,z) == 0) 
          zero_voxel_count2++;
      }
    }
  }

  // std::cout << "compute negative free space score for " << count 
  // << " voxels" << std::endl;

  std::cout << "compute negative free space score for " << 
  zero_voxel_count - zero_voxel_count2 << " voxels" << std::endl;
}

std::string matrix3d::toString() const {
  std::string rtn = "\n";
  for (int i = 0; i < z_size(); ++i)
  {
    for (int j = 0; j < y_size(); ++j)
    {
      for (int k = 0; k < x_size(); ++k)
      {
        rtn = rtn + std::to_string(this->get(k,j,i)) + " ";
      }
      rtn = rtn + "\n";
    }
    rtn = rtn + "\n";
  }
  return rtn;
}

