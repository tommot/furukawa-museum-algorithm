#ifndef FSC_TL_H
#define FSC_TL_H

/**
 * @file fsc.h
 * @author Tom Lai <tomlai@.berkeley.edu>
 *
 * @section DESCRIPTION
 *
 * This file contains classes used to manipulate and compute free space score 
 * in furukawa reconstruction algorithm.
 *
 * Make sure to compile with -std=c++11 to support C++11 standard.
 * 
 * Requires Eigen3 and PCL library to compile correctly.
 *
 */

#include <string>
#include <iostream>
#include <vector>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/octree/octree.h>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Geometry>

#define PI 3.14159265

namespace fsc
{
  // tolerance degree for measuring parallel / perpendicular lines
  const double degree_of_tolerance = 5.0;

  /* the following classes are defined in this file */
  class line_2d;
  class line_3d;
  class matrix3d;

  inline bool in_range(double x, double start, double end) { return start <= x 
    and x <= end;}
  inline double max3(double x, double y, double z) {return fmax(fmax(x,y),z);}
  inline double min3(double x, double y, double z) {return fmin(fmin(x,y),z);}

  class line_2d
  {
    public:
      Eigen::Vector2d point;
      Eigen::Vector2d displacement;
      // constructor with two end points
      line_2d(double x1, double y1, double x2, double y2);
      line_2d(Eigen::Vector2d point, Eigen::Vector2d displacement);

      inline Eigen::Vector2d end_point()
        {return this->point + this->displacement;}

      // return the slope of the line
      inline double slope() 
        {return this->displacement[1]/this->displacement[0];}

      // return the slope of the line, two same lines along the opposite directions 
      // will return same slope from this function, rather than a +/- sign difference
      inline double degree() 
        {return atan(this->displacement[1]/this->displacement[0]) * 180 / PI;}

      inline double length() 
        {return sqrt(pow(this->displacement[1],2) + pow(this->displacement[0],2));}

      line_2d perpendicular_line();

      // solve for _intersection between two lines
      bool has_intersection(line_2d line);
      Eigen::Vector2d get_intersection(line_2d line);

      // check if a line is a subline for another line, with a small degree of
      // tolerance
      bool is_parallel_to(line_2d line);
      bool is_perpendicular_to(line_2d line);

      std::string toString() const;
      friend std::ostream& operator<<(std::ostream& outs, const line_2d& obj) {
        return outs << obj.toString();
      }
  };

  class line_3d
  {
    public:
      Eigen::Vector3d point;
      Eigen::Vector3d displacement;
      inline Eigen::Vector3d end_point() {return this->point + 
        this->displacement;}
      line_3d(Eigen::Vector3d point, Eigen::Vector3d displacement);
      std::string toString() const;
      friend std::ostream& operator<<(std::ostream& outs, const line_3d& obj) {
        return outs << obj.toString();
      }
  };

  class matrix3d
  {
    private:
      int x,y,z;
      inline bool is_valid_position(int x, int y, int z) {return in_range(x,0,
        this->x_size()), in_range(y,0,this->y_size()), in_range(z,0,
        this->z_size());}

    public:
      int x_size() const;
      int y_size() const;
      int z_size() const;
      std::vector< Eigen::MatrixXd > matrix;
      matrix3d(int x, int y, int z);
      //~matrix3d();
      void increment_for_line(line_3d line);
      inline void increment(int x, int y, int z) { this->matrix[z](x,y) ++; }
      inline void set(int x, int y, int z, double value) { this->matrix[z](x,y) = 
        value; }
      inline double get(int x, int y, int z) const { return this->matrix[z](x,y); }
      inline void set_z_layer(int z, Eigen::MatrixXd m) {this->matrix[z] = m;}
      inline Eigen::MatrixXd get_z_layer(int z) { return this->matrix[z];}
      void enforce_max_value(int value);
      void compute_negative_free_space_score(double resolution, double factor);
      static void* compute_negative_free_space_score_parallel(void *inputs);
      std::string toString() const;
      friend std::ostream& operator<<(std::ostream& outs, const matrix3d& obj) {
        return outs << obj.toString();
      }
  };
}

inline bool operator==(const fsc::line_2d& l1, const fsc::line_2d& l2){ return l1.point == l2.point and l1.displacement == l2.displacement; }

#endif
